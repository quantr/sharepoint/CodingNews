import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
	BaseClientSideWebPart,
	IPropertyPaneConfiguration,
	PropertyPaneTextField,
	PropertyPaneDropdown,
	PropertyPaneToggle,
	PropertyPaneButton,
	PropertyPaneButtonType,
	IWebPartContext
} from '@microsoft/sp-webpart-base';

import * as strings from 'CodingNewsWebPartStrings';
import CodingNews from './components/CodingNews';
import { ICodingNewsProps } from './components/ICodingNewsProps';
import { SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions } from '@microsoft/sp-http';

export interface ICodingNewsWebPartProps {
	// description: string;
}

export default class CodingNewsWebPart extends BaseClientSideWebPart<ICodingNewsWebPartProps> {

	public render(): void {
		const element: React.ReactElement<ICodingNewsProps> = React.createElement(
			CodingNews,
			{
				// description: this.properties.description,
				pageContext: this.context.pageContext,
				spHttpClient: this.context.spHttpClient
			}
		);

		ReactDom.render(element, this.domElement);
	}

	protected onDispose(): void {
		ReactDom.unmountComponentAtNode(this.domElement);
	}

	protected get dataVersion(): Version {
		return Version.parse('1.0');
	}

	protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
		return {
			pages: [
				{
					header: {
						description: strings.PropertyPaneDescription
					},
					groups: [
						{
							groupName: strings.BasicGroupName,
							groupFields: [
								PropertyPaneButton('Icon List button', {
									text: "Icon List",
									buttonType: PropertyPaneButtonType.Normal,
									onClick: this.handleClick.bind(this, 'Icon List'),
									icon: 'Group'
								}),
								PropertyPaneButton('Jump to List Icon', {
									text: "Jump to List Icon",
									buttonType: PropertyPaneButtonType.Normal,
									onClick: this.handleClick.bind(this, 'Jump to List Icon'),
									icon: 'Group'
								}),
							]
						}
					]
				}
			]
		};
	}

	private async handleClick(value) {
		if (value == "Icon List") {
			var opt: ISPHttpClientOptions = {};
			var httpResponse = await this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl + "/_api/Web/Lists?$filter=title eq 'Icon'", SPHttpClient.configurations.v1, opt);
			var response = await httpResponse;
			var json: any = await response.json();

			// check list exist or not
			if (json.value.length == 0) {
				// create list
				opt = {
					body: `{
								AllowContentTypes: true,
								BaseTemplate: 100,
								Description: 'Storing website icons',
								Title: 'Icon'
							}`
				};
				httpResponse = await this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + "/_api/web/lists", SPHttpClient.configurations.v1, opt);
				if (httpResponse.status != 201) {
					alert('Unable to create list');
					return;
				}

				//create drop down column, kelvin read this then you know FieldTypeKind: https://msdn.microsoft.com/en-us/library/microsoft.sharepoint.client.fieldtype.aspx
				response = await httpResponse;
				var json: any = await response.json();
				opt = {
					body: `{
								'@odata.type': 'SP.FieldChoice',
								FieldTypeKind: 6,
								Title: 'Behaviour',
								StaticName: 'Behaviour',
								Required: true,
								Choices: ['Choice1', 'Choice2', 'Choice3']
							}`
				};
				httpResponse = await this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('Icon')/Fields", SPHttpClient.configurations.v1, opt);
				response = await httpResponse;
				if (httpResponse.status != 201) {
					alert('Unable to create columns');
					return;
				}


				//create single line of text column
				response = await httpResponse;
				var json: any = await response.json();
				opt = {
					body: `{
								'@odata.type': 'SP.FieldText',
								FieldTypeKind: 2,
								Title: 'First Name',
								StaticName: 'First Name',
								Required: true
							}`
				};
				httpResponse = await this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('Icon')/Fields", SPHttpClient.configurations.v1, opt);
				response = await httpResponse;
				if (httpResponse.status != 201) {
					alert('Unable to create columns');
					return;
				}


				//create multiple line of text column
				response = await httpResponse;
				var json: any = await response.json();
				opt = {
					body: `{
								'@odata.type': 'SP.FieldText',
								FieldTypeKind: 3,
								Title: 'Address',
								StaticName: 'Address',
								Required: true
							}`
				};
				httpResponse = await this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('Icon')/Fields", SPHttpClient.configurations.v1, opt);
				response = await httpResponse;
				if (httpResponse.status != 201) {
					alert('Unable to create columns');
					return;
				}

				await this.addFieldToView('Icon', 'Behaviour');
				await this.addFieldToView('Icon', 'First Name');
				await this.addFieldToView('Icon', 'Address');

				alert('List `Icon` created successfully');

				// opt = {};
				// httpResponse = await this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('Icon')/views/getbytitle('All Items')/ViewFields/AddViewField('Behaviour')", SPHttpClient.configurations.v1, opt);
				// response = await httpResponse;
				// if (httpResponse.status == 204) {
				// 	alert('List `Icon` created successfully');
				// } else {
				// 	alert('Unable to add column to view');
				// }
			} else {
				alert('List `Icon` already exists');
			}
		} else if (value == "Jump to List Icon") {
			window.open(this.context.pageContext.web.absoluteUrl + '/Lists/Icon');
		}
	}

	private async addFieldToView(listName: string, fieldName: string) {
		var opt: ISPHttpClientOptions = {};
		var httpResponse = await this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('" + listName + "')/views/getbytitle('All Items')/ViewFields/AddViewField('" + fieldName + "')", SPHttpClient.configurations.v1, opt);
		var response: any = await httpResponse;
	}
}
