import * as React from 'react';
import styles from './CodingNews.module.scss';
import { ICodingNewsProps } from './ICodingNewsProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions } from '@microsoft/sp-http';
import { DefaultButton, IButtonProps } from 'office-ui-fabric-react/lib/Button';
import {
	DetailsList,
	DetailsListLayoutMode,
	Selection,
	SelectionMode,
	IColumn
} from 'office-ui-fabric-react/lib/DetailsList';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';

// interface IDocument {
// 	ID: string;
// 	Title: string;
// 	Date: string;
// 	Body: string;
// 	URL: string;
// 	From: string;
// }

export default class CodingNews extends React.Component<ICodingNewsProps, { showNews: string, showItems: string }> {
	private items: any = [];
	private buttons = ["Home", "High level", "Low level", "Java", 'NodeJS', 'C#'];
	private searchStrings = ["", "java,c#,nodejs", "c++,assembly", 'java', 'nodejs', 'c#'];

	constructor(props) {
		super(props);

		this.state = { showNews: 'none', showItems: '' };

		this.loadList('');
	}

	public render(): React.ReactElement<ICodingNewsProps> {
		var classNames: any = require('classnames');
		var buttonComponents = [];
		for (var x = 0; x < this.buttons.length; x++) {
			buttonComponents.push(
				<DefaultButton primary={true} onClick={this.loadList.bind(this, this.searchStrings[x])}>
					{this.buttons[x]}
				</DefaultButton>
			);
		}
		return (
			<div className={styles.codingNews}>
				<div className={styles.headerBar}>
					{buttonComponents}
					<DefaultButton text="Back" onClick={this.handleBack} style={{ display: this.state.showNews }} />
				</div>
				<div>
					<SearchBox
						placeholder="Search"
						onChange={this.search}
						underlined={true}
					/>
				</div>
				<div className="ms-Grid" style={{ display: this.state.showItems }}>
					{this.items}
				</div>
				<div style={{ display: this.state.showNews, marginTop: '5px' }}>
					<div className="ms-Grid">
						<div className="ms-Grid-row">
							<div style={{ display: this.state.showNews }} className={classNames("ms-Grid-col")} ref="newsBody"></div>
						</div>
					</div>
				</div>
			</div >
		);
	}

	private showNews(id: string): void {
		var bodyDiv: HTMLElement = this.refs.newsBody as HTMLElement;

		const opt: ISPHttpClientOptions = {};
		this.props.spHttpClient.get(this.props.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('News')/items(" + id + ")", SPHttpClient.configurations.v1, opt).then((response: SPHttpClientResponse) => {
			response.json().then((json: any) => {
				this.setState({ showNews: '' });
				this.setState({ showItems: 'none' });
				bodyDiv.innerHTML = json.Body;
				this.forceUpdate();
			});
		});
	}

	private handleBack = () => {
		this.setState({ showNews: 'none' });
		this.setState({ showItems: '' });
	}

	private search = (value) => {
		this.loadList(value);
	}

	private loadList(searchString) {
		this.handleBack();
		const opt: ISPHttpClientOptions = {};
		this.props.spHttpClient.get(this.props.pageContext.web.absoluteUrl + "/_api/web/lists/getbytitle('News')/items?$orderby=Date desc", SPHttpClient.configurations.v1, opt).then((response: SPHttpClientResponse) => {
			response.json().then((json: any) => {
				this.items = [];
				var classNames: any = require('classnames');
				//var xx=;
				var lastData;
				for (var x = 0; x < json.value.length; x++) {
					var bingo = false;
					for (var s2 of searchString.split(',')) {
						if (json.value[x].Title.toLowerCase().includes(s2.toLowerCase()) || json.value[x].Body.toLowerCase().includes(s2.toLowerCase())) {
							bingo = true;
						}
					}
					if (bingo) {
						var timeDiff = Math.abs(new Date(json.value[x].Date).getTime() - new Date().getTime());
						var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						var days;
						if (diffDays == 0) {
							days = <span style={{ color: 'green' }}>Today</span>;
						} else if (diffDays == 1) {
							days = <span style={{ color: 'green' }}>Yesterday</span>;
						} else if (diffDays < 7) {
							days = <span style={{ color: 'green' }}>This week</span>;
						} else {
							days = <span style={{ color: 'green' }}>Over week</span>;
						}
						if (lastData != diffDays) {
							this.items.push(
								<div className="ms-Grid-row" style={{ paddingTop: '10px', paddingLeft: '10px', paddingBottom: '5px', cursor: 'pointer' }} onClick={this.showNews.bind(this, json.value[x].Id)}>
									<div className={classNames("ms-Grid-col")}>
										<div className={styles.date} >{days}</div>
									</div>
								</div>
							);
							lastData = diffDays;
						}
						var img;
						if (json.value[x].From.indexOf("codementor.io") != -1) {
							img = <img src={require('./image/codeMentor.png')} className={styles.brandLogo} />;
						} else if (json.value[x].From.indexOf("infoq.com") != -1) {
							img = <img src={require('./image/infoq.png')} className={styles.brandLogo} />;
						} else if (json.value[x].From.indexOf("javacodegeeks.com") != -1) {
							img = <img src={require('./image/javaCodeGeeks.png')} className={styles.brandLogo} />;
						} else if (json.value[x].From.indexOf("microsoft.com") != -1) {
							img = <img src={require('./image/microsoft.png')} className={styles.brandLogo} />;
						}
						this.items.push(
							<div className={classNames("ms-Grid-row", styles.rowSpan)} style={{ paddingTop: '10px', paddingLeft: '25px', paddingBottom: '10px', cursor: 'pointer' }} onClick={this.showNews.bind(this, json.value[x].Id)}>
								<div className={classNames("ms-Grid-col ms-sm1 ms-md2 ms-hiddenMdDown")}>
									{img}
								</div>
								<div className={classNames("ms-Grid-col ms-sm11 ms-md10")}>
									<div className={classNames("ms-Grid-row")}>
										<div className={classNames("ms-Grid-col")}>
											<div className={styles.date} >{json.value[x].Date.replace('T', ' ').replace('Z', ' ')}</div>
										</div>
										<div className={classNames("ms-Grid-col")}>
											<div className={styles.from}>{json.value[x].From}</div>
										</div>
									</div>
									<div className={classNames("ms-Grid-row")}>
										<div className={classNames("ms-Grid-col")}>
											<div className={styles.title}>
												{json.value[x].Title}
											</div>
										</div>
									</div>
								</div>
							</div>
						);
					}
				}
				//this.items = this._sortItems(this.items, 'MembershipNo');
				this.forceUpdate();
			});
		});
	}
}
