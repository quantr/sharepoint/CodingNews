import { PageContext } from "@microsoft/sp-page-context";
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';

export interface ICodingNewsProps {
	pageContext: PageContext;
	spHttpClient: SPHttpClient;
}
