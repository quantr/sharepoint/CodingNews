declare interface ICodingNewsWebPartStrings {
	PropertyPaneDescription: string;
	BasicGroupName: string;
}

declare module 'CodingNewsWebPartStrings' {
	const strings: ICodingNewsWebPartStrings;
	export = strings;
}
